package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.example.project_jsp.domain.CD;
import com.example.project_jsp.domain.cdTrackList;

public final class editTrack_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
      out.write("\"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("\r\n");
      com.example.project_jsp.service.StorageService storage = null;
      synchronized (application) {
        storage = (com.example.project_jsp.service.StorageService) _jspx_page_context.getAttribute("storage", PageContext.APPLICATION_SCOPE);
        if (storage == null){
          storage = new com.example.project_jsp.service.StorageService();
          _jspx_page_context.setAttribute("storage", storage, PageContext.APPLICATION_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      com.example.project_jsp.domain.CD CD = null;
      synchronized (session) {
        CD = (com.example.project_jsp.domain.CD) _jspx_page_context.getAttribute("CD", PageContext.SESSION_SCOPE);
        if (CD == null){
          CD = new com.example.project_jsp.domain.CD();
          _jspx_page_context.setAttribute("CD", CD, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("\r\n");

    int idCD = Integer.parseInt(request.getParameter("idCD"));
    int idTrack = Integer.parseInt(request.getParameter("idTrack"));
    CD cd = storage.getAllCD().get(idCD);
    cdTrackList track = cd.getTrackLists().get(idTrack);

    pageContext.setAttribute("cd", cd);

      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("    <!-- CSS -->\r\n");
      out.write("    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "import.jsp", out, false);
      out.write("\r\n");
      out.write("    <title>IMusic</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("<nav class=\"navbar navbar-inverse\">\r\n");
      out.write("    <div class=\"container-fluid\">\r\n");
      out.write("        <div class=\"navbar-header\">\r\n");
      out.write("            <a class=\"navbar-brand\" href=\"index.jsp\">IMusic</a>\r\n");
      out.write("        </div>\r\n");
      out.write("        <ul class=\"nav navbar-nav\">\r\n");
      out.write("            <li><a href=\"index.jsp\">Home</a></li>\r\n");
      out.write("            <li><a href=\"showAllCDs.jsp\">Library</a></li>\r\n");
      out.write("            <li><a href=\"getCDData.jsp\">Add New CD</a></li>\r\n");
      out.write("        </ul>\r\n");
      out.write("    </div>\r\n");
      out.write("</nav>\r\n");
      out.write("<div class=\"container-fluid full\">\r\n");
      out.write("    <div class=\"jumbotron\">\r\n");
      out.write("        <h2>U wanna edit ");
      out.print(track.getNameOfTrack());
      out.write("!</h2>\r\n");
      out.write("        <form action=\"submitTrackUpdate.jsp?idCD=");
      out.print(idCD);
      out.write("&idTrack=");
      out.print(idTrack);
      out.write("\">\r\n");
      out.write("            <input type=\"hidden\" name=\"idCD\" value=\"");
      out.print(idCD);
      out.write("\">\r\n");
      out.write("            <input type=\"hidden\" name=\"idTrack\" value=\"");
      out.print(idTrack);
      out.write("\">\r\n");
      out.write("            <div class=\"form-group\">\r\n");
      out.write("                <label for=\"nameOfTrack\">Name Of Track</label>\r\n");
      out.write("                <input type=\"text\" class=\"form-control\" id=\"nameOfTrack\" name=\"nameOfTrack\" value=\"");
      out.print(track.getNameOfTrack());
      out.write("\">\r\n");
      out.write("            </div>\r\n");
      out.write("            <button type=\"submit\" value=\"Edit\" class=\"btn btn-default\">Edit!</button>\r\n");
      out.write("        </form>\r\n");
      out.write("        <a href=\"cdDetails.jsp?id=");
      out.print(idCD);
      out.write("\" class=\"btn btn-default\" role=\"button\">Back</a>\r\n");
      out.write("    </div>\r\n");
      out.write("</div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
