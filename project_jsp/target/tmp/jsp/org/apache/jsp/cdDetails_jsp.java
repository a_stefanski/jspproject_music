package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.example.project_jsp.domain.CD;

public final class cdDetails_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_varStatus_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_varStatus_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_varStatus_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      com.example.project_jsp.service.StorageService storage = null;
      synchronized (application) {
        storage = (com.example.project_jsp.service.StorageService) _jspx_page_context.getAttribute("storage", PageContext.APPLICATION_SCOPE);
        if (storage == null){
          storage = new com.example.project_jsp.service.StorageService();
          _jspx_page_context.setAttribute("storage", storage, PageContext.APPLICATION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("\r\n");
 int id = Integer.parseInt(request.getParameter("id"));
    CD cd = storage.getAllCD().get(id);
    pageContext.setAttribute("cd", cd);
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("    <title>IMusic | ");
      out.print(cd.getNameOfCD());
      out.write("</title>\r\n");
      out.write("    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "import.jsp", out, false);
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Confirmation.html", out, false);
      out.write("\r\n");
      out.write("\r\n");
      out.write("<nav class=\"navbar navbar-inverse\">\r\n");
      out.write("    <div class=\"container-fluid\">\r\n");
      out.write("        <div class=\"navbar-header\">\r\n");
      out.write("            <a class=\"navbar-brand\" href=\"\">IMusic</a>\r\n");
      out.write("        </div>\r\n");
      out.write("        <ul class=\"nav navbar-nav\">\r\n");
      out.write("            <li><a href=\"index.jsp\">Home</a></li>\r\n");
      out.write("            <li class=\"active\"><a href=\"showAllCDs.jsp\">Library</a></li>\r\n");
      out.write("            <li><a href=\"getCDData.jsp\">Add New CD</a></li>\r\n");
      out.write("        </ul>\r\n");
      out.write("    </div>\r\n");
      out.write("</nav>\r\n");
      out.write("\r\n");
      out.write("<div class=\"container-fluid\">\r\n");
      out.write("    <div class=\"jumbotron\">\r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("\r\n");
      out.write("            <div class=\"row\">\r\n");
      out.write("                <div class=\"col-xs-6 col-md-3\">\r\n");
      out.write("                    <a href=\"\" class=\"thumbnail\">\r\n");
      out.write("                        <img src=\"");
      out.print(cd.getImg());
      out.write("\" alt=\"Ameba\">\r\n");
      out.write("                    </a>\r\n");
      out.write("                    <div class=\"panel panel-default\">\r\n");
      out.write("                        <!-- Default panel contents -->\r\n");
      out.write("                        <div class=\"panel-heading\"><b>");
      out.print(cd.getNameOfCD());
      out.write("</b></div>\r\n");
      out.write("                        <!-- List group -->\r\n");
      out.write("                        <ul class=\"list-group\">\r\n");
      out.write("                            <li class=\"list-group-item\">Release date: ");
      out.print(cd.getYoc());
      out.write("</li>\r\n");
      out.write("                            <li class=\"list-group-item\">Author: ");
      out.print(cd.getAuthor());
      out.write("</li>\r\n");
      out.write("                            <li class=\"list-group-item\">Count of Tracks: ");
      out.print(cd.getCountOfTracks());
      out.write("</li>\r\n");
      out.write("                            <li class=\"list-group-item\">Genre: ");
      out.print(cd.getGenre());
      out.write("</li>\r\n");
      out.write("                            <li class=\"list-group-item\">Price: ");
      out.print(cd.getPrice());
      out.write("$</li>\r\n");
      out.write("                        </ul>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"col-xs-6 col-md-9\">\r\n");
      out.write("                    <p class=\"lead\">");
      out.print(cd.getDescription());
      out.write("</p>\r\n");
      out.write("                    <table class=\"table table-condensed\">\r\n");
      out.write("                        <tr class=\"active\">\r\n");
      out.write("                            <th><strong>Track List</strong></th>\r\n");
      out.write("                            <th></th>\r\n");
      out.write("                        </tr>\r\n");
      out.write("\r\n");
      out.write("                            ");
      //  c:forEach
      org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_varStatus_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
      _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
      _jspx_th_c_forEach_0.setParent(null);
      _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cd.getTrackLists()}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
      _jspx_th_c_forEach_0.setVar("trackList");
      _jspx_th_c_forEach_0.setVarStatus("loop");
      int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
      try {
        int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
        if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
          do {
            out.write("\r\n");
            out.write("                                <tr>\r\n");
            out.write("\r\n");
            out.write("                                    <td><h5 style=\"font-size: larger\">");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${trackList.getNameOfTrack()}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write(" </h5></td>\r\n");
            out.write("                                    <td>\r\n");
            out.write("                                        <a href=\"editTrack.jsp?idCD=");
            out.print(id);
            out.write("&idTrack=");
            out.print(cd.getTrackLists().indexOf(pageContext.getAttribute("trackList")));
            out.write("\" class=\"btn btn-info\" role=\"button\">Edit Track</a>\r\n");
            out.write("                                        <button class=\"btn btn-danger\" data-href=\"deleteTrack.jsp?idCD=");
            out.print(id);
            out.write("&idTrack=");
            out.print(cd.getTrackLists().indexOf(pageContext.getAttribute("trackList")));
            out.write("\" data-toggle=\"modal\" data-target=\"#confirm-delete-track\">\r\n");
            out.write("                                            Delete Track\r\n");
            out.write("                                        </button>\r\n");
            out.write("                                    </td>\r\n");
            out.write("                                </tr>\r\n");
            out.write("                            ");
            int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
            if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
              break;
          } while (true);
        }
        if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
          return;
        }
      } catch (Throwable _jspx_exception) {
        while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
          out = _jspx_page_context.popBody();
        _jspx_th_c_forEach_0.doCatch(_jspx_exception);
      } finally {
        _jspx_th_c_forEach_0.doFinally();
        _jspx_tagPool_c_forEach_varStatus_var_items.reuse(_jspx_th_c_forEach_0);
      }
      out.write("\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td>\r\n");
      out.write("                                <a href=\"addTrack.jsp?id=");
      out.print(id);
      out.write("\" class=\"btn btn-default\" role=\"button\">Add new track</a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                    <div class=\"col-xs-6 col-md-3\">\r\n");
      out.write("                        <a href=\"showAllCDs.jsp\" class=\"btn btn-default\" role=\"button\">Back</a>\r\n");
      out.write("                        <a href=\"editCD.jsp?id=");
      out.print(id);
      out.write("\" class=\"btn btn-info\" role=\"button\">Edit CD</a>\r\n");
      out.write("                        <button class=\"btn btn-danger\" data-href=\"deleteCD.jsp?id=");
      out.print(id);
      out.write("\" data-toggle=\"modal\" data-target=\"#confirm-delete\">\r\n");
      out.write("                            Delete CD\r\n");
      out.write("                        </button>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <!-- Delete CD script -->\r\n");
      out.write("                    <script>\r\n");
      out.write("                        $('#confirm-delete').on('show.bs.modal', function(e) {\r\n");
      out.write("                            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));\r\n");
      out.write("                        });\r\n");
      out.write("\r\n");
      out.write("                        $('#confirm-delete-track').on('show.bs.modal', function(e) {\r\n");
      out.write("                            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));\r\n");
      out.write("                        });\r\n");
      out.write("                    </script>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
