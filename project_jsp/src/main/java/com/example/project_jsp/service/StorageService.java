package com.example.project_jsp.service;

import java.util.ArrayList;
import java.util.List;



import com.example.project_jsp.domain.CD;
import com.example.project_jsp.domain.cdTrackList;

public class StorageService {
	
	private static List<CD> db = new ArrayList<CD>();

	private CD changedCD(CD cd)
	{
		String nameOfCD = cd.getNameOfCD().substring(0,1).toUpperCase() + cd.getNameOfCD().substring(1).toLowerCase();
		String author = cd.getAuthor().substring(0,1).toUpperCase() + cd.getAuthor().substring(1).toLowerCase();
		return new CD(nameOfCD,cd.getYoc(),author,cd.getImg(), cd.getCountOfTracks(), cd.getGenre(), cd.getPrice(), cd.getDescription());
	}

	public void add(CD cd){
		CD newCD = new CD(cd.getNameOfCD(), cd.getYoc(), cd.getAuthor(), cd.getImg(), cd.getCountOfTracks(), cd.getGenre(), cd.getPrice(), cd.getDescription());
		db.add(newCD);
	}
	
	public List<CD> getAllCD()
	{
		return db;
	}
	public List<CD> All()
	{
		return db;
	}

	public void addCDTrack(int id, cdTrackList cdTrackList)
	{
		db.get(id).getTrackLists().add(cdTrackList);
	}



	public void addTrack(int idCD, cdTrackList track)
	{
		db.get(idCD).getTrackLists().add(track);
	}

	public void deleteCD(int id)
	{
		db.remove(id);
	}

	public void deleteTrack(int idTrack, int idCD)
	{
		db.get(idCD).getTrackLists().remove(idTrack);
	}

	public void updateCD(int id, CD cd)
	{
		CD updatedCD = changedCD(cd);
		updatedCD.setTrackLists(db.get(id).getTrackLists());
		db.set(id,updatedCD);
	}

	public void updateTrack(int idCD, int idTrack, cdTrackList track)
	{
		cdTrackList updatedTrack = new cdTrackList(track.getNameOfTrack());
		db.get(idCD).getTrackLists().set(idTrack,updatedTrack);
	}

	public void addExample()
	{
		CD cd1 = new CD("Ameba", "30.09.2016", "Gedz", "http://polskirap.com.pl/wp-content/uploads/gedz-ameba-okladka.jpg",6,"Rap", 30, "Album sklada sie z czternastu utworow, w ktorych goscinnie uslyszec mozna takich raperow jak: Hades, Rak Raczej, W.E.N.A, Malolat czy Paluch. Za warstwe muzyczna plyty odpowiadaja: Robert Dziedowicz, Grrracz, Deemz, Hvzx, Beat Sampras, CatchUp, Gedz, Małe Miasta, Hnsn1 i ENZU.");
		cd1.addTrack("Dach (prod. Robert Dziedowicz)");
		cd1.addTrack("Freon (prod. CatchUp)");
		cd1.addTrack("Placebo (prod. Robert Dziedowicz)");
		cd1.addTrack("Szafa Pełna Kicksów ft. Małolat, W.E.N.A. (prod. Grrracz)");
		cd1.addTrack("Matrix ft. RakRaczej, Hades, DJ Krug (prod. Beat Sampras)");
		cd1.addTrack("Inny Biegun (prod. Gedz & Małe Miasta)");
		db.add(cd1);

		CD cd2 = new CD("NNJL", "24.10.2014", "Gedz", "https://proxy1.pebx.eu/?url=polskirap.com.pl%2Fwp-content%2Fuploads%2Fgedz-nnjl-okladka.jpg",6,"Rap", 25, "Album studyjny polskiego rapera Gedza. Wydawnictwo ukazalo sie 24 pazdziernika 2014 roku nakladem wytworni muzycznej Urban Rec. Produkcji nagran podjeli sie: Robert Dziedowicz, Grrracz, Deemz, Sherlock, Julas oraz Henson. Wikipedia");
		cd2.addTrack("Mgła (feat. DJ Krug)");
		cd2.addTrack("Akrofobia");
		cd2.addTrack("C.G.W.D. (Intro)");
		cd2.addTrack("C.G.W.D.");
		cd2.addTrack("Molotov");
		cd2.addTrack("S&C");
		db.add(cd2);

		CD cd3 = new CD("Walls", "10.10.2016", "Kings Of Leon", "http://ocs-pl.oktawave.com/v1/AUTH_2887234e-384a-4873-8bc5-405211db13a2/splay/2016/10/kings_of_leon_walls.jpg",10,"Rock", 38.95, "Jedna z najpopularniejszych grup rockowych - KINGS OF LEON powraca z nowa plyta! \"WALLS\" to tytul siodmego albumu studyjnego. Jego premiera zaplanowana jest na 14 pazdziernika nakladem RCA Records. Nagrania odbyly sie w studio w Los Angeles przy wspolpracy ze slynnym producentem Markusem Dravs (Arcade Fire, Coldplay, Florence + the Machine).");
		cd3.addTrack("Waste A Moment");
		cd3.addTrack("Reverend");
		cd3.addTrack("Around The World");
		cd3.addTrack("Find Me");
		cd3.addTrack("Over");
		cd3.addTrack("Muchacho");
		cd3.addTrack("Conversation Piece");
		cd3.addTrack("Eyes On You");
		cd3.addTrack("Wild");
		cd3.addTrack("Walls");
		db.add(cd3);

		CD cd4 = new CD("Nothing But The Beat", "26.08.2011", "David Guetta", "https://i.ytimg.com/vi/inNgROXbzZc/hqdefault.jpg",13,"Pop", 28.95, "After the huge success of Platinum and NRJ Music Award winning album One Love! 6 Dance Smashes, 4 Top 5 Airplay hits and Top 10 single Hits, the Master of Dance, David Guetta, is back! With his fifth studio album \"Nothing But The Beat\" and already 2 Dance Smash hits! \"Where Them Girls At\" and \"Little Bad Girls\".");
		cd4.addTrack("Titanium");
		cd4.addTrack("Turn Me On");
		cd4.addTrack("She Wolf (Falling To Pieces)");
		cd4.addTrack("Without You");
		cd4.addTrack("I Can Only Imagine");
		cd4.addTrack("Play Hard");
		cd4.addTrack("Wild One 2 (Edit.)");
		cd4.addTrack("Just One Last Time");
		cd4.addTrack("In My Head");
		cd4.addTrack("Little Bad Girl");
		cd4.addTrack("Sweat");
		cd4.addTrack("Crank It Up");
		cd4.addTrack("Nothing Really Matters");
		db.add(cd4);

		CD cd5 = new CD("Last Stand", "19.08.2016", "Sabaton", "https://upload.wikimedia.org/wikipedia/en/6/6a/Sabaton_-_The_Last_Stand_cover.jpg",11,"Metal", 29.99, "Szwedzcy metalowcy z Sabaton ukonczyli prace nad swoim kolejnym krazkiem, \"The Last Stand\", ktorego premiere zapowiedziano na 19 sierpnia 2016 roku.\n" +
				"Zespol po raz kolejny zdecydowal sie na wspolprace z Peterem Tagtgrenem w slynnym Abyss Studio.");
		cd5.addTrack("Sparta");
		cd5.addTrack("Last Dying Breath");
		cd5.addTrack("Blood Of Bannockburn");
		cd5.addTrack("Diary Of An Unknown Soldier");
		cd5.addTrack("The Lost Battalion");
		cd5.addTrack("Muchacho");
		cd5.addTrack("Rorke's Drift");
		cd5.addTrack("The Last Stand");
		cd5.addTrack("Shiroyama");
		cd5.addTrack("Winged Hussars");
		cd5.addTrack("The Last Battle");
		db.add(cd5);
	}


}
