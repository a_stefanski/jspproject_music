package com.example.project_jsp.web;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.project_jsp.domain.CD;
import com.example.project_jsp.service.StorageService;


@WebServlet(name = "searchGenre", urlPatterns = "/searchGenre")

public class searchGenre extends HttpServlet {
    StorageService storage = new StorageService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<CD> searchedCDs = new ArrayList<CD>();

        String searchingCDsByGenre = request.getParameter("genre");

        int i = 0;
        int isSearched = 0;

        if(storage.getAllCD().size() > 0)
        {

            for(CD cd : storage.getAllCD())
            {
                if(cd.getGenre().equals(searchingCDsByGenre))
                {
                    searchedCDs.add(cd);
                    i++;
                    isSearched = 1;
                }
            }
        }

        response.setContentType("text/html");

        request.setAttribute("searchedCDs", searchedCDs);
        request.setAttribute("isSearched", isSearched);
        request.setAttribute("genre", searchingCDsByGenre);
        request.getRequestDispatcher("/searchedCDsByGenre.jsp").forward(request,response);

    }
}
