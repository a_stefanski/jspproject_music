package com.example.project_jsp.domain;

import java.util.ArrayList;
import java.util.List;

public class CD {
	
	private String nameOfCD;
	private String  yoc ;
	private String author ;
	private String img;
	private List<cdTrackList> trackLists;
	private int countOfTracks;
	private String genre;
	private double price;
	private String description;

	public CD() {
		super();
	}
	
	public CD(String nameOfCD, String yoc, String author, String img, int countOfTracks, String genre, double price, String description) {
		super();
		this.nameOfCD = nameOfCD;
		this.yoc = yoc;
		this.author = author;
		this.img = img;
		this.countOfTracks = countOfTracks;
		this.genre = genre;
		this.price = price;
		this.description = description;

		trackLists = new ArrayList<cdTrackList>();
	}

	public String getNameOfCD() {
		return nameOfCD;
	}
	public void setNameOfCD(String nameOfCD) {
		this.nameOfCD = nameOfCD;
	}
	public String getYoc() {
		return yoc;
	}
	public void setYoc(String yoc) {
		this.yoc = yoc;
	}
	public String getAuthor()
	{
		return author;
	}
	public void setAuthor(String author)
	{
		this.author = author;
	}
	public String getImg(){return img;}
	public void setImg(String img){this.img = img;}
	public int getCountOfTracks(){ return countOfTracks;}
	public void setCountOfTracks(int countOfTracks){this.countOfTracks = countOfTracks;}
	public String getGenre(){return genre;}
	public void setGenre(String genre){this.genre = genre;}
	public double getPrice(){return price;}
	public void setPrice(double price){this.price = price;}
	public String getDescription(){return description;}
	public void setDescription(String description){this.description = description;}

	public List<cdTrackList> getTrackLists()
	{
		return trackLists;
	}

	public void setTrackLists(List<cdTrackList> trackLists)
	{
		this.trackLists = trackLists;
	}

	public void addTrack(String nameOfTrack)
	{
		cdTrackList trackList = new cdTrackList(nameOfTrack);
		trackLists.add(trackList);
	}
}
