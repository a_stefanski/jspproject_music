package com.example.project_jsp.domain;

/**
 * Created by Alex on 2016-11-01.
 */
public class cdTrackList {
    private String nameOfTrack;

    public cdTrackList() {
        super();
    }

    public cdTrackList(String nameOfTrack)
    {
        super();
        this.nameOfTrack = nameOfTrack;
    }


    public String getNameOfTrack()
    {
        return nameOfTrack;
    }
    public void setNameOfTrack(String nameOfTrack)
    {
        this.nameOfTrack = nameOfTrack;
    }
}
