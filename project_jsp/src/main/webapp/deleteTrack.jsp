<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<jsp:useBean id="cd" class="com.example.project_jsp.domain.CD" scope="session" />

<jsp:setProperty name="cd" property="*" />

<jsp:useBean id="storage" class="com.example.project_jsp.service.StorageService" scope="application" />

<%
    int idCD = Integer.parseInt(request.getParameter("idCD"));
    int idTrack = Integer.parseInt(request.getParameter("idTrack"));
    storage.deleteTrack(idTrack,idCD);
    response.sendRedirect("cdDetails.jsp?id=" + idCD);
%>
