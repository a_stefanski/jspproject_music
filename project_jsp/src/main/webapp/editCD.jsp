<%@ page import="com.example.project_jsp.domain.CD" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="storage" class="com.example.project_jsp.service.StorageService" scope="application" />
<jsp:useBean id="CD" class="com.example.project_jsp.domain.CD" scope="session" />

<% int id = Integer.parseInt(request.getParameter("id"));
    CD cd = storage.getAllCD().get(id);
    pageContext.setAttribute("cd", cd);%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- CSS -->
    <jsp:include page="import.jsp"/>
    <title>IMusic</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.jsp">IMusic</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="index.jsp">Home</a></li>
            <li><a href="showAllCDs.jsp">Library</a></li>
            <li><a href="getCDData.jsp">Add New CD</a></li>
        </ul>
    </div>
</nav>
<div class="container-fluid full">



    <div class="jumbotron">
        <h2>U wanna edit ${cd.nameOfCD}!</h2>




        <form action="submitCDUpdate.jsp?id=${id}">
            <input type="hidden" name="id" value="<%=id%>">
            <div class="form-group">
                <label for="nameOfCD">Name of CD</label>
                <input type="text" class="form-control" id="nameOfCD" name="nameOfCD" value="<%=cd.getNameOfCD()%>">
            </div>
            <div class="form-group">
                <label for="yoc">Year of Create</label>
                <input type="text" class="form-control" id="yoc" name="yoc" value="<%=cd.getYoc()%>">
            </div>
            <div class="form-group">
                <label for="author">Author</label>
                <input type="text" class="form-control" id="author" name="author" value="<%=cd.getAuthor()%>">
            </div>
            <div class="form-group">
                <label for="img">IMG</label>
                <input type="text" class="form-control" id="img" name="img" value="<%=cd.getImg()%>">
            </div>
            <div class="form-group">
                <label for="countOfTracks">Count Of Tracks</label>
                <input type="text" class="form-control" id="countOfTracks" name="countOfTracks" value="<%=cd.getCountOfTracks()%>">
            </div>
            <div class="form-group">
                <label for="genre">Genre</label>
                <input type="text" class="form-control" id="genre" name="genre" value="<%=cd.getGenre()%>">
            </div>
            <div class="form-group">
                <label for="genre">Price</label>
                <input type="text" class="form-control" id="price" name="price" value="<%=cd.getPrice()%>">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" class="form-control" id="description" name="description" value="<%=cd.getDescription()%>">
            </div>
            <button type="submit" value="Edit" class="btn btn-default">Edit!</button>
        </form>
        <a href="cdDetails.jsp?id=<%=id%>" class="btn btn-default" role="button">Back</a>
    </div>
</div>
</body>
</html>
