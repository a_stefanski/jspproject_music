<%@page import="com.example.project_jsp.domain.CD" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:useBean id="storage" class="com.example.project_jsp.service.StorageService" scope="application"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% int id = Integer.parseInt(request.getParameter("id"));
    CD cd = storage.getAllCD().get(id);
    pageContext.setAttribute("cd", cd);%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- CSS -->
    <jsp:include page="import.jsp"/>
    <title>IMusic</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.jsp">IMusic</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="index.jsp">Home</a></li>
            <li><a href="showAllCDs.jsp">Library</a></li>
            <li class="active"><a href="getCDData.jsp">Add New CD</a></li>
        </ul>
    </div>
</nav>
<div class="container-fluid full">



    <div class="jumbotron">
        <h2>Add track list to ${cd.nameOfCD}</h2>

        <form action="addTracks.jsp">
            <c:forEach var="i" begin="1" end="${cd.countOfTracks}">
                <label for="track${i}">Track ${i}</label>
                <input type="text" class="form-control" id="track${i}" name="track${i}" placeholder="Track${i}">
            </c:forEach>
            <button type="submit" value="add" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
</body>
</html>