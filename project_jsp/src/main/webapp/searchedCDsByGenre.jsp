<%@page import="com.example.project_jsp.domain.CD"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="storage" class="com.example.project_jsp.service.StorageService" scope="application" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
    int counter = 1;
%>


<html lang="pl-PL">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- CSS -->
    <jsp:include page="import.jsp"/>
    <title>IMusic</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.jsp">IMusic</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="index.jsp">Home</a></li>
            <li class="active"><a href="showAllCDs.jsp">Library</a></li>
            <li><a href="getCDData.jsp">Add New CD</a></li>
        </ul>
    </div>
</nav>

<div class="container marketing">

    <!-- http://v4-alpha.getbootstrap.com/examples/carousel/-->
    <c:if test="${storage.All().size() == 0}">
        <div class="container-fluid">
            <h1>Library empty?</h1>
            <p><a class="btn btn-default btn-lg" href="addExampleCDs.jsp">Add example CD's</a></p>
        </div>
    </c:if>

    <hr class="featurette-divider">
    <c:if test="${ isSearched == 1}">
        <c:forEach items="${searchedCDs}" var="cd">
            <div class="row featurette">
                <div class="col-md-7">
                    <a href="cdDetails.jsp?id=${storage.All().indexOf(cd)}">
                        <h2 class="featurette-heading"> <span class="text-muted"><%=counter%></span> "${cd.getNameOfCD()}" - ${cd.getAuthor()}</h2>
                    </a>
                    <p class="lead">Genre: ${cd.getGenre()}</p>
                    <p class="lead">Price: ${cd.getPrice()}$</p>
                    <p class="lead">Release date: ${cd.getYoc()}</p>
                    <p class="lead">${cd.getDescription()}</p>
                </div>
                <div class="col-md-5">
                    <a href="cdDetails.jsp?id=${storage.All().indexOf(cd)}">
                        <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="${cd.getImg()}" width="500">
                    </a>
                </div>
            </div>
            <%
                counter++;
            %>
            <hr class="featurette-divider">
        </c:forEach>
    </c:if>
    <c:if test="${ isSearched == 0}">
        <div class="container-fluid">
            <h1>The library does not have any CDs with that genre: ${genre}</h1>
        </div>
    </c:if>

    <!-- /END COPY DIVS FROM BOOTSTRAP EXAMPLES -->


    <!-- FOOTER -->
    <footer>
        <p class="float-xs-right"><a href="showAllCDs.jsp">Back to library!</a></p>
        <p>&copy; 2016 Aleksander Stefański</p>
    </footer>

</div><!-- /.container -->


</body>
</html>
