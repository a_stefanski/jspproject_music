<%@page import="com.example.project_jsp.domain.CD" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:useBean id="storage" class="com.example.project_jsp.service.StorageService" scope="application"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% int id = Integer.parseInt(request.getParameter("id"));
    CD cd = storage.getAllCD().get(id);
    pageContext.setAttribute("cd", cd);%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>IMusic | <%=cd.getNameOfCD()%></title>
    <jsp:include page="import.jsp"/>
</head>
<body>
<jsp:include page="Confirmation.html"/>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="">IMusic</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="index.jsp">Home</a></li>
            <li class="active"><a href="showAllCDs.jsp">Library</a></li>
            <li><a href="getCDData.jsp">Add New CD</a></li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="jumbotron">
        <div class="row">

            <div class="row">
                <div class="col-xs-6 col-md-3">
                    <a href="" class="thumbnail">
                        <img src="<%=cd.getImg()%>" alt="Ameba">
                    </a>
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading"><b><%=cd.getNameOfCD()%></b></div>
                        <!-- List group -->
                        <ul class="list-group">
                            <li class="list-group-item">Release date: <%=cd.getYoc()%></li>
                            <li class="list-group-item">Author: <%=cd.getAuthor()%></li>
                            <li class="list-group-item">Count of Tracks: <%=cd.getCountOfTracks()%></li>
                            <li class="list-group-item">Genre: <%=cd.getGenre()%></li>
                            <li class="list-group-item">Price: <%=cd.getPrice()%>$</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-6 col-md-9">
                    <p class="lead"><%=cd.getDescription()%></p>
                    <table class="table table-condensed">
                        <tr class="active">
                            <th><strong>Track List</strong></th>
                            <th></th>
                        </tr>

                            <c:forEach items="${cd.getTrackLists()}" var="trackList" varStatus="loop">
                                <tr>

                                    <td><h5 style="font-size: larger">${trackList.getNameOfTrack()} </h5></td>
                                    <td>
                                        <a href="editTrack.jsp?idCD=<%=id%>&idTrack=<%=cd.getTrackLists().indexOf(pageContext.getAttribute("trackList"))%>" class="btn btn-info" role="button">Edit Track</a>
                                        <button class="btn btn-danger" data-href="deleteTrack.jsp?idCD=<%=id%>&idTrack=<%=cd.getTrackLists().indexOf(pageContext.getAttribute("trackList"))%>" data-toggle="modal" data-target="#confirm-delete-track">
                                            Delete Track
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                        <tr>
                            <td>
                                <a href="addTrack.jsp?id=<%=id%>" class="btn btn-default" role="button">Add new track</a>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                    <div class="col-xs-6 col-md-3">
                        <a href="showAllCDs.jsp" class="btn btn-default" role="button">Back</a>
                        <a href="editCD.jsp?id=<%=id%>" class="btn btn-info" role="button">Edit CD</a>
                        <button class="btn btn-danger" data-href="deleteCD.jsp?id=<%=id%>" data-toggle="modal" data-target="#confirm-delete">
                            Delete CD
                        </button>
                    </div>

                    <!-- Delete CD script -->
                    <script>
                        $('#confirm-delete').on('show.bs.modal', function(e) {
                            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
                        });

                        $('#confirm-delete-track').on('show.bs.modal', function(e) {
                            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>