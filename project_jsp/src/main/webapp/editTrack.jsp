<%@ page import="com.example.project_jsp.domain.CD" %>
<%@ page import="com.example.project_jsp.domain.cdTrackList" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="storage" class="com.example.project_jsp.service.StorageService" scope="application" />
<jsp:useBean id="CD" class="com.example.project_jsp.domain.CD" scope="session" />

<%
    int idCD = Integer.parseInt(request.getParameter("idCD"));
    int idTrack = Integer.parseInt(request.getParameter("idTrack"));
    CD cd = storage.getAllCD().get(idCD);
    cdTrackList track = cd.getTrackLists().get(idTrack);

    pageContext.setAttribute("cd", cd);
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- CSS -->
    <jsp:include page="import.jsp"/>
    <title>IMusic</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.jsp">IMusic</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="index.jsp">Home</a></li>
            <li><a href="showAllCDs.jsp">Library</a></li>
            <li><a href="getCDData.jsp">Add New CD</a></li>
        </ul>
    </div>
</nav>
<div class="container-fluid full">
    <div class="jumbotron">
        <h2>U wanna edit <%=track.getNameOfTrack()%>!</h2>
        <form action="submitTrackUpdate.jsp?idCD=<%=idCD%>&idTrack=<%=idTrack%>">
            <input type="hidden" name="idCD" value="<%=idCD%>">
            <input type="hidden" name="idTrack" value="<%=idTrack%>">
            <div class="form-group">
                <label for="nameOfTrack">Name Of Track</label>
                <input type="text" class="form-control" id="nameOfTrack" name="nameOfTrack" value="<%=track.getNameOfTrack()%>">
            </div>
            <button type="submit" value="Edit" class="btn btn-default">Edit!</button>
        </form>
        <a href="cdDetails.jsp?id=<%=idCD%>" class="btn btn-default" role="button">Back</a>
    </div>
</div>
</body>
</html>
