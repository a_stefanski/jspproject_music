<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<jsp:useBean id="cd" class="com.example.project_jsp.domain.CD" scope="session" />

<jsp:setProperty name="cd" property="*" />

<jsp:useBean id="storage" class="com.example.project_jsp.service.StorageService" scope="application" />

<%

    String trackText;
    int id = storage.getAllCD().size() - 1;
    for (int i=1; i <= storage.getAllCD().get(id).getCountOfTracks(); i++)
    {
        trackText = request.getParameter("track" + i);
        storage.getAllCD().get(id).addTrack(trackText);
    }
    response.sendRedirect("cdDetails.jsp?id=" + id);
%>