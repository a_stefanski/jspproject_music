<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- CSS -->
        <jsp:include page="import.jsp"/>
        <title>IMusic</title>
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="">IMusic</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="">Home</a></li>
                    <li><a href="showAllCDs.jsp">Library</a></li>
                    <li><a href="getCDData.jsp">Add New CD</a></li>
                </ul>
            </div>
        </nav>
        <div class="container-fluid full">



            <div class="jumbotron">
                <h1>Welcome on my page!</h1>
                <p>Want to check new popular CD's?</p>
                <p><a class="btn btn-primary btn-lg" href="showAllCDs.jsp" role="button">Click here</a></p>
            </div>
        </div>
    </body>
</html>
