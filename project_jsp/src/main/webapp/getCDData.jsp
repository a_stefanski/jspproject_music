<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- CSS -->
  <jsp:include page="import.jsp"/>
  <title>IMusic</title>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.jsp">IMusic</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="index.jsp">Home</a></li>
      <li><a href="showAllCDs.jsp">Library</a></li>
      <li class="active"><a href="getCDData.jsp">Add New CD</a></li>
    </ul>
  </div>
</nav>
<div class="container-fluid full">



  <div class="jumbotron">
    <h2>Add new CD!</h2>
    <jsp:useBean id="storage" class="com.example.project_jsp.service.StorageService" scope="application" />
    <jsp:useBean id="CD" class="com.example.project_jsp.domain.CD" scope="session" />



    <form action="addCD.jsp">
      <div class="form-group">
        <label for="nameOfCD">Name of CD</label>
        <input type="text" class="form-control" id="nameOfCD" name="nameOfCD">
      </div>
      <div class="form-group">
        <label for="yoc">Year of Create</label>
        <input type="text" class="form-control" id="yoc" name="yoc">
      </div>
      <div class="form-group">
        <label for="author">Author</label>
        <input type="text" class="form-control" id="author" name="author">
      </div>
      <div class="form-group">
        <label for="img">IMG</label>
        <input type="text" class="form-control" id="img" name="img">
      </div>
      <div class="form-group">
        <label for="countOfTracks">Count Of Tracks</label>
        <input type="text" class="form-control" id="countOfTracks" name="countOfTracks">
      </div>
      <div class="form-group">
        <label for="genre">Genre</label>
        <input type="text" class="form-control" id="genre" name="genre">
      </div>
      <div class="form-group">
        <label for="genre">Price</label>
        <input type="text" class="form-control" id="price" name="price">
      </div>
      <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" id="description" name="description">
      </div>

      <button type="submit" value="add" class="btn btn-default">Submit</button>
      </form>
      <a href="showAllCDs.jsp" class="btn btn-default" role="button">Back</a>
  </div>
</div>
</body>
</html>
