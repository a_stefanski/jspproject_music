<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:useBean id="cd" class="com.example.project_jsp.domain.CD" scope="session" />

<jsp:setProperty name="cd" property="*" /> 

<jsp:useBean id="storage" class="com.example.project_jsp.service.StorageService" scope="application" />

<%
  storage.add(cd);
  int id = storage.getAllCD().size() - 1;
  response.sendRedirect("getTrackData.jsp?id=" + id);
%>
